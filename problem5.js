const problem5 = (year) => {
  const olderCars = [];
  for (let index = 0; index < year.length; index++) {
    // Looping through the year array
    if (year[index] < 2000) {
      // Checking if the car_year is less than 2000
      olderCars.push(year[index]);
    }
  }
  return [olderCars, olderCars.length];
};

module.exports = problem5;
