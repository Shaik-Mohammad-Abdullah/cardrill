const problem4 = (data) => {
  const year = [];
  for (let index = 0; index < data.length; index++) {
    // Appending the year array through looping over the object 
    year.push(data[index].car_year);
  }
  return year;
};

module.exports = problem4;
