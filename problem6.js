const problem6 = (data) => {
  const BMWAndAudi = [];
  for (let index = 0; index < data.length; index++) {
    if (data[index].car_make === "Audi" || data[index].car_make === "BMW") {
      // Checking if the Car is either Audi or BMW
      BMWAndAudi.push(data[index]);
    }
  }
  // Printing the Audi and BMW cars in JSON format
  return JSON.stringify(BMWAndAudi);
};

module.exports = problem6;
