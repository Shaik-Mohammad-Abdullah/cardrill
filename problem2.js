const problem2 = (lastData) => {
  // Returns the last object data
  return `Last car is a ${lastData.car_make} ${lastData.car_model}`;
};

module.exports = problem2; 