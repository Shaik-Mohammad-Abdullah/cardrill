const problem3 = (data) => {
  const carModel = [];
  for (let index = 0; index < data.length; index++) {
    // Appending the carModel array by looping through the object
    carModel.push(data[index].car_model);
  }
  for (let rowIndex = 0; rowIndex < carModel.length; rowIndex++) {
    for (let columnIndex = 0; columnIndex < carModel.length - rowIndex - 1; columnIndex++) {
      if (carModel[columnIndex + 1] < carModel[columnIndex]) {
        // Sorting the carModel array
        [carModel[columnIndex + 1], carModel[columnIndex]] = [
          carModel[columnIndex],
          carModel[columnIndex + 1],
        ];
      }
    }
  }
  return carModel;
};

module.exports = problem3;
