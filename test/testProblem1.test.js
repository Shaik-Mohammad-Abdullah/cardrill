const problem1 = require("../problem1");
const data = require("../inventory");
const { expect } = require("@jest/globals");

// Reading the inputs from the data provided by inventory

test("Will return the sentence of car_id 33", () => {
  expect(problem1(data, 33)).toBe("Car 33 is a 2011 Jeep Wrangler");
});
