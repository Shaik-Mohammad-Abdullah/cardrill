const problem5 = require("../problem5");
const data = require("../inventory");
const { test, expect } = require("@jest/globals");
const problem4 = require("../problem4");

// Reading the inputs from the data provided by inventory

const expectedOutput = [
  [
    1983, 1990, 1995, 1987, 1996, 1997, 1999, 1987, 1995, 1994, 1985, 1997,
    1992, 1993, 1964, 1999, 1991, 1997, 1992, 1998, 1965, 1996, 1995, 1996,
    1999,
  ],
  25,
];

test("Will return car models built before 2000", () => {
  expect(problem5(problem4(data))).toStrictEqual(expectedOutput);
});
