const problem2 = require("../problem2");
const data = require("../inventory");
const { expect, test } = require("@jest/globals");

// Reading the inputs from the data provided by inventory

lastData = data.pop();
// Reading the last data

test("Will result with the last car data", () => {
    expect(problem2(lastData)).toBe("Last car is a Lincoln Town Car");
});