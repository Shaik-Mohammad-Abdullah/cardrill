const problem1 = (data, carId) => {
  for (let index = 0; index < data.length; index++) {
    // Looping through the objects to check for the desired id
    if (data[index].id === carId) {
      // Car 33 is a 2011 Jeep Wrangler
      return (`Car 33 is a ${data[index].car_year} ${data[index].car_make} ${data[index].car_model}`);
    }
  }
};

module.exports = problem1;
